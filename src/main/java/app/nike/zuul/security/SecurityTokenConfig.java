package app.nike.zuul.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import app.nike.zuul.filters.JwtTokenAuthenticationFilter;


@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter
{
	@Autowired
	private JwtConfig jwtConfig;

	private static final String[] USER_MATCHER = { "/nike/**"};
	private static final String[] ADMIN_MATCHER = { "/api/**" };
	

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.csrf().disable()
				//La sezione deve essere STATELESS
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.exceptionHandling()
				.authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)).and()
				// Aggiunge un filtro di validazione dei token ad ogni richiesta
				.addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
				 //Precedura di autorizzazione
				.authorizeRequests()
				// Abilita l'URL POST specificato nel config
				.antMatchers(HttpMethod.POST, "/auth/**").permitAll()
				 //Matcher di autenticazione
				.antMatchers(USER_MATCHER).hasAnyRole("USER")
				.antMatchers(ADMIN_MATCHER).hasAnyRole("ADMIN")
				//Qualsiasi altra richiesta deve essere autenticata
				.anyRequest().authenticated();
				
	}

	@Override
    public void configure(WebSecurity web) throws Exception {
        web
          .ignoring()
          .antMatchers(HttpMethod.POST,"/auth/**");

        //completely bypass the Spring Security Filter Chain.
    }
	
	/*
	/@Bean
	public JwtConfig jwtConfig()
	{
		return new JwtConfig();
	}
	*/
}
