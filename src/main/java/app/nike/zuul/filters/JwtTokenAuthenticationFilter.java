package app.nike.zuul.filters;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import app.nike.zuul.security.JwtConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter
{
	private static final Logger logger = LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);
	
	private final JwtConfig jwtConfig;

	public JwtTokenAuthenticationFilter(JwtConfig jwtConfig)
	{
		this.jwtConfig = jwtConfig;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException
	{

		// 1. otteniamo l'authentication header. Tokens risiede nell'header
		String header = request.getHeader(jwtConfig.getHeader());

		// 2. validazione dell'header e controllo del prefix
		if (header == null || !header.startsWith(jwtConfig.getPrefix()))
		{
			chain.doFilter(request, response); //Se non valido prosegue con il filtro seguente 
												
			return;
		}

		// If there is no token provided and hence the user won't be
		// authenticated.
		// It's Ok. Maybe the user accessing a public path or asking for a
		// token.

		// All secured paths that needs a token are already defined and secured
		// in config class.
		// And If user tried to access without access token, then he won't be
		// authenticated and an exception will be thrown.

		// 3. Otteniamo il token
		String token = header.replace(jwtConfig.getPrefix(), "");
		
		logger.warn("token: " + token);

		try
		{  //se il token è scaduto viena lanciata una eccezione
			 

			// 4. Validazione del token
			Claims claims = Jwts.parser()
					.setSigningKey(jwtConfig.getSecret().getBytes())
					.parseClaimsJws(token)
					.getBody();

			String username = claims.getSubject();
			
			logger.warn("Utente: " + username);
			
			if (username != null)
			{
				@SuppressWarnings("unchecked")
				List<String> authorities = (List<String>) claims.get("authorities");

				// 5. Creazione della classe  auth
				// UsernamePasswordAuthenticationToken: A built-in object, used
				// by spring to represent the current authenticated / being
				// authenticated user.
				// It needs a list of authorities, which has type of
				// GrantedAuthority interface, where SimpleGrantedAuthority is
				// an implementation of that interface
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, null,
						authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

				// 6.Autenticazione dell'utente
				// Now, user is authenticated
				SecurityContextHolder.getContext().setAuthentication(auth);
			}

		} 
		catch (Exception e)
		{
			logger.error(e.getMessage());

			SecurityContextHolder.clearContext();
		}

		// passa al filtro successivo della catena
		chain.doFilter(request, response);
	}
}
