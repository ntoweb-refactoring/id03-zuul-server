FROM openjdk:8-jdk-alpine
VOLUME [ "/tmp" ]
ARG JAVA_OPTS
ENV JAVA_OPTS=${JAVA_OPTS}
ADD target/*.jar id03-zuul-server.jar
EXPOSE 8765
ENTRYPOINT exec java ${JAVA_OPTS} -jar id03-zuul-server.jar
